Provides a data input and CIM-compliant field extractions for Microsoft Sysmon. The Microsoft Sysmon utility provides data on process creation (including parent process ID), network connections, and much more.

This add-on was originally created by Adrian Hall. We appreciate Adrian's contribution and his willingness to turn over control to the current team for ongoing maintenance and development.