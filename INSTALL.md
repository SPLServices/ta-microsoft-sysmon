## Pre-implementation 

Complete deployment for Windows Base PT-OS-001-Microsoft-Windows
Deploy Microsoft sysmon 6.x (refrain from later) to all endpoints and servers using the customers internally approved tools and methods. If established patterns do not exist review the following article for guidance [https://p0w3rsh3ll.wordpress.com/2015/04/21/deploy-sysmon-with-powershell-desired-state-configuration/]() 

Review, update and deploy as appropriate ``sysmon-config/sysmoncfg_v31.xml from the bitbucket repository``

## Data Acquisition Procedure Microsoft Windows 7/2008R2 +

Data collection for operational use cases including Windows Infra Structure App and general active directory functional monitoring
Deployment Servers

* Deploy to apps ``SecKit_all_deploymentserver_3_ms_sysmon``
* Stage the following apps to deployment-apps
	* TA-microsoft-sysmon
	* TA-microsoft-sysmon_seckit_0_all_inputs
	* Reload Deployment Server